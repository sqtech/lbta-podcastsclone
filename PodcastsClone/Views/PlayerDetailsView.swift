//
//  PlayerDetailsView.swift
//  PodcastsClone
//
//  Created by Simon Quach on 2018-05-26.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit
import AVKit
import MediaPlayer

protocol PlayerDetailsViewDelegate: class {
  func playerDetailsViewMinimizeView(_ playerDetailsView: PlayerDetailsView)
  func playerDetailsViewMaximizeView(_ playerDetailsView: PlayerDetailsView)
}

class PlayerDetailsView: UIView {
  static func initFromNib() -> PlayerDetailsView {
    return Bundle.main.loadNibNamed("PlayerDetailsView", owner: self, options: nil)?.first as! PlayerDetailsView
  }

  enum PlayerMode {
    case play, paused, notPlaying
  }

  // MARK: Properties
  var episode: Episode! {
    didSet {
      miniEpisodeTitleLabel.text = episode.title
      episodeTitleLabel.text = episode.title
      authorLabel.text = episode.author

      setupNowPlayingInfoForLockScreen()
      // Resets the layout to not playing mode
      updatePlayerLayout(for: .notPlaying)

      setupAudioSession()
      playEpisode()
      guard let url = URL(string: episode.imageUrl ?? "") else { return }
      episodeImageView.sd_setImage(with: url)
      miniEpisodeImageView.sd_setImage(with: url)
      miniEpisodeImageView.sd_setImage(with: url) { (image, _, _, _) in

        let image = self.episodeImageView.image ?? UIImage()
        let artwork = MPMediaItemArtwork(boundsSize: .zero, requestHandler: { _ -> UIImage in
          return image
        })
        MPNowPlayingInfoCenter.default().nowPlayingInfo?[MPMediaItemPropertyArtwork] = artwork
      }
    }
  }

  var playListEpisodes = [Episode]()

  weak var delegate: PlayerDetailsViewDelegate?

  lazy var player: AVPlayer = {
    let avPlayer = AVPlayer()
    avPlayer.automaticallyWaitsToMinimizeStalling = false
    return avPlayer
  }()

  let shrunkenTransform = CGAffineTransform(scaleX: 0.7, y: 0.7)

  var panGesture: UIPanGestureRecognizer!

  // MARK: Full Player View Outlet Properties
  @IBOutlet weak var maximizedStackView: UIStackView!
  @IBOutlet weak var episodeImageView: UIImageView! {
    didSet {
      episodeImageView.layer.cornerRadius = 4
      episodeImageView.clipsToBounds = true
      animateEpisodeImageView(with: shrunkenTransform)
    }
  }

  @IBOutlet weak var currentTimeSlider: UISlider!
  @IBOutlet weak var currentTimeLabel: UILabel!
  @IBOutlet weak var durationTimeLabel: UILabel!
  @IBOutlet weak var episodeTitleLabel: UILabel!
  @IBOutlet weak var authorLabel: UILabel!
  @IBOutlet weak var playPauseButton: UIButton! {
    didSet {
      playPauseButton.addTarget(self, action: #selector(handlePlayPause), for: .touchUpInside)
    }
  }

  @IBOutlet weak var rewindButton: UIButton! {
    didSet {
      rewindButton.addTarget(self, action: #selector(handleRewind), for: .touchUpInside)
    }
  }

  @IBOutlet weak var forwardButton: UIButton! {
    didSet {
      forwardButton.addTarget(self, action: #selector(handleFastForward), for: .touchUpInside)
    }
  }

  // MARK: Mini Player View Outlet Properties
  @IBOutlet weak var miniPlayerView: UIView!
  @IBOutlet weak var miniEpisodeImageView: UIImageView!
  @IBOutlet weak var miniEpisodeTitleLabel: UILabel!
  @IBOutlet weak var miniPlayPauseButton: UIButton! {
    didSet {
      miniPlayPauseButton.addTarget(self, action: #selector(handlePlayPause), for: .touchUpInside)
      miniPlayPauseButton.imageEdgeInsets = .init(top: 8, left: 8, bottom: 8, right: 8)
    }
  }
  @IBOutlet weak var miniFastForwardButton: UIButton! {
    didSet {
      miniFastForwardButton.addTarget(self, action: #selector(handleFastForward), for: .touchUpInside)
      miniFastForwardButton.imageEdgeInsets = .init(top: 8, left: 8, bottom: 8, right: 8)
    }
  }


  // MARK: Override Methods
  override func awakeFromNib() {
    super.awakeFromNib()
    setupRemoteControl()
    observePlayerCurrentTime()
    observeBoundaryTime()
    setupGestures()
    setupInteruptionObserver()

  }

  deinit {
    print("deinitializing PlayerDetailsView")
    NotificationCenter.default.removeObserver(self, name: Notification.Name.AVAudioSessionInterruption, object: nil)
  }

}

// MARK: Setup and Layout Methods
extension PlayerDetailsView {
  func updatePlayerLayout(for playerMode: PlayerMode) {
    switch playerMode {
    case .paused, .notPlaying:
      playPauseButton.setImage(#imageLiteral(resourceName: "play"), for: .normal)
      miniPlayPauseButton.setImage(#imageLiteral(resourceName: "play"), for: .normal)
      animateEpisodeImageView(with: shrunkenTransform)
    case .play:
      playPauseButton.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
      miniPlayPauseButton.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
      animateEpisodeImageView(with: .identity)
    }
  }

  /// The slider min = 0, max = 1. Dividing the current time over the duration gives an approximate %
  func updateCurrentTimeSlider() {
    let currentTimeSeconds = CMTimeGetSeconds(player.currentTime())
    let durationTimeSeconds = CMTimeGetSeconds(player.currentItem?.duration ?? CMTimeMake(1, 1))
    let percentage = currentTimeSeconds / durationTimeSeconds
    currentTimeSlider.value = Float(percentage)
//    currentTimeSlider.maximumValue = Float(CMTimeGetSeconds(player.currentItem?.duration ?? CMTimeMake(0, 0)))
  }

  func observeBoundaryTime() {
    let time = CMTimeMake(1, 3)
    let times = [NSValue(time: time)]
    // Allows to monitor player when it starts
    // Retain cycle issue:
    // player has a STRONG reference to self, so if self is removed from super view the player will still be playing
    // by adding weak self, the reference of player will be removed if self is removed
    // self has a reference to player (above in declaration closure)
    // need to break out with [weak self]
    player.addBoundaryTimeObserver(forTimes: times, queue: .main) { [weak self] in
      print("Episode started playing")
      self?.updatePlayerLayout(for: .play)
      self?.setupLockscreenDuration()
    }
  }


  /// This is called every half second
  func observePlayerCurrentTime() {
    let interval = CMTimeMake(1, 2) // every half second?
    player.addPeriodicTimeObserver(forInterval: interval, queue: .main, using: { [weak self] time in
      self?.currentTimeLabel.text = time.toDisplayString()
      let durationTime = self?.player.currentItem?.duration
      self?.durationTimeLabel.text = durationTime?.toDisplayString()
      self?.updateCurrentTimeSlider()
    })
  }

  func animateEpisodeImageView(with transform: CGAffineTransform) {
    UIView.animate(withDuration: 0.75, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
      // .identity = Goes back to scale 1 (from 0.7)
      self.episodeImageView.transform = transform
    })
  }

  func playEpisode() {
//    print("Playing episode at url:", episode.streamUrl)
    var url: URL

    // Use downloaded file URL to play episode
    if let fileURLString = episode.fileUrl {
      print("Attempting to play episode with file URL: \(fileURLString)")
      guard let fileURL = getTrueLocationURL(from: fileURLString) else { return }
      url = fileURL

    } else { // Stream from URL
      guard let streamURL = URL(string: episode.streamUrl) else { return }
      url = streamURL
    }

    let playerItem = AVPlayerItem(url: url)
    player.replaceCurrentItem(with: playerItem)
    player.play()
  }

  /// Find the "TRUE" location where the episode file is
  /// - parameter file: The file from which the downloaded episode.fileURL is stored in
  func getTrueLocationURL(from file: String) -> URL? {
    guard
      var trueLocationURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first,
      let fileURL = URL(string: file)
      else { print("No true location URL"); return nil }

    let fileName = fileURL.lastPathComponent
    trueLocationURL.appendPathComponent(fileName)
    return trueLocationURL
  }

  func setupInteruptionObserver() {
    NotificationCenter.default.addObserver(self, selector: #selector(handleInteruption), name: Notification.Name.AVAudioSessionInterruption, object: nil)
  }

  func setupGestures() {
    addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTapMaximize)))
    panGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
    miniPlayerView.addGestureRecognizer(panGesture)

    maximizedStackView.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handleDismissalPan)))
  }
}

// MARK: Interuption Handler
private extension PlayerDetailsView {
  @objc func handleInteruption(notification: Notification) {
    guard let userInfo = notification.userInfo else { return }
    guard let type = userInfo[AVAudioSessionInterruptionTypeKey] as? UInt else { return }

    // When there is an interuption, the audio is paused by default
    if type == AVAudioSessionInterruptionType.began.rawValue {
      updatePlayerLayout(for: .paused)
    } else if type == AVAudioSessionInterruptionType.ended.rawValue {
      // interruption type ended can occur after user finishes call, or comes back into app after navigating out of it
      guard let options = userInfo[AVAudioSessionInterruptionOptionKey] as? UInt else { return }

      // Checks whether app should resume playback after call has ended
      if options == AVAudioSessionInterruptionOptions.shouldResume.rawValue {
        player.play()
        updatePlayerLayout(for: .play)
      }

    }
  }
}

// MARK: Player Control Handlers
extension PlayerDetailsView {
  @IBAction func handleCurrentTimeSlider(_ sender: Any) {
    print("Slider value: ", currentTimeSlider.value)
    let percentage = currentTimeSlider.value
    guard let duration = player.currentItem?.duration else { return }

    let durationInSeconds = CMTimeGetSeconds(duration)
    let seekTimeInSeconds = Float64(percentage) * durationInSeconds
    let seekTime = CMTimeMakeWithSeconds(seekTimeInSeconds, 1)
    setupLockscreenElapsedTime(for: seekTimeInSeconds)
    player.seek(to: seekTime)
  }

  @objc func handleRewind() {
    seekToCurrentTime(withDelta: -15)
  }

  @objc func handleFastForward() {
    seekToCurrentTime(withDelta: 15)
  }

  @IBAction func handleVolumeChange(_ sender: UISlider) {
    player.volume = sender.value
  }

  @objc func handleNextTrack() {
    if playListEpisodes.count == 0 {
      return
    }
    guard let currentEpisodeIndex = playListEpisodes.index(where: { episode in
      return self.episode.title == episode.title && self.episode.author == episode.author
    }) else { return }

    let nextEpisode: Episode
    // If episode is at the very bottom, wrap back up to the top
    if currentEpisodeIndex == playListEpisodes.count - 1 {
      nextEpisode = playListEpisodes[0]
    } else {
      nextEpisode = playListEpisodes[currentEpisodeIndex + 1]
    }

    self.episode = nextEpisode
    
  }

  @objc func handlePreviousTrack() {
    if playListEpisodes.count == 0 {
      return
    }
    guard let currentEpisodeIndex = playListEpisodes.index(where: { episode in
      return self.episode.title == episode.title && self.episode.author == episode.author
    }) else { return }

    let previousEpisode: Episode
    // If episode is at the very top, go to bottom
    if currentEpisodeIndex == 0 {
      previousEpisode = playListEpisodes[playListEpisodes.count - 1]
    } else {
      previousEpisode = playListEpisodes[currentEpisodeIndex - 1]
    }

    self.episode = previousEpisode
  }

  @objc func handlePlayPause() {
    switch player.timeControlStatus {
    case .paused:
      player.play()
      updatePlayerLayout(for: .play)
      setupLockscreenElapsedTime(playbackRate: 1)
    case .playing:
      player.pause()
      updatePlayerLayout(for: .paused)
      setupLockscreenElapsedTime(playbackRate: 0)
    case .waitingToPlayAtSpecifiedRate:
      return
    }
  }

  func seekToCurrentTime(withDelta delta: Int64) {
    let currentTime = player.currentTime()
    let fifteenSeconds = CMTimeMake(delta, 1)
    let seekTime = CMTimeAdd(currentTime, fifteenSeconds)
    let seekTimeInSeconds = CMTimeGetSeconds(seekTime)
    setupLockscreenElapsedTime(for: seekTimeInSeconds)
    player.seek(to: seekTime)
  }
}

// MARK: Gesture Methods
extension PlayerDetailsView {
  /// Handles the pan gesture for the mini player view
  @objc func handlePan(gesture: UIPanGestureRecognizer) {
    //    print("Handle pan:", gesture.state)
    let translation = gesture.translation(in: self.superview)
    let velocity = gesture.velocity(in: self.superview)

    switch gesture.state {
    case .changed:
//      print(translation.y)
      self.transform = CGAffineTransform(translationX: 0, y: translation.y)

      // at y = 200 height, alpha will be 0
      self.miniPlayerView.alpha = 1 + translation.y / 200
      self.maximizedStackView.alpha = -translation.y / 200
    case .ended:
      UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
        self.transform = .identity // Go back to original state
        // y is moving higher up, or flick velocity is fast then maximize it
        if translation.y < -200 || velocity.y < -500 {
          self.handleTapMaximize()
        } else {
          self.handleDismiss(self)
        }

      })
    default:
      break
    }
  }

  /// Handles the pan gesture for the maximized player view
  @objc func handleDismissalPan(gesture: UIPanGestureRecognizer) {
    let translation = gesture.translation(in: self.superview)
    let velocity = gesture.velocity(in: self.superview)

    switch gesture.state {
    case .changed:
      self.maximizedStackView.transform = CGAffineTransform(translationX: 0, y: translation.y)
    case .ended:
      UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
        // y is moving down, or flick in y > 500
        self.maximizedStackView.transform = .identity

        if translation.y > 50 || velocity.y > 500 {
          self.handleDismiss(self)
        }

      })
    default:
      break
    }
  }

  /// Shrinks the player details view above the tab bar
  @IBAction func handleDismiss(_ sender: Any) {
    delegate?.playerDetailsViewMinimizeView(self)
  }

  /// Maximizes the player view to cover the whole screen
  @objc func handleTapMaximize() {
    delegate?.playerDetailsViewMaximizeView(self)
  }

}
