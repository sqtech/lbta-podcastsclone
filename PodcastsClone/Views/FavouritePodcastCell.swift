//
//  FavouritePodcastCell.swift
//  PodcastsClone
//
//  Created by Simon Quach on 2018-06-17.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit

class FavouritePodcastCell: UICollectionViewCell {
  var podcast: Podcast? {
    didSet {
      updateLayout(with: podcast)
    }
  }

  let imageView: UIImageView = {
    let iv = UIImageView()
    iv.image = #imageLiteral(resourceName: "appicon")
    iv.contentMode = .scaleAspectFill
    iv.clipsToBounds = true
    return iv
  }()

  let nameLabel: UILabel = {
    let label = UILabel()
    label.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
    label.text  = "TEST TITLE"
    return label
  }()

  let artistNameLabel: UILabel = {
    let label = UILabel()
    label.font = UIFont.systemFont(ofSize: 12, weight: .semibold)
    label.text  = "TEST ARTIST"
    label.textColor = .lightGray
    return label
  }()

  override init(frame: CGRect) {
    super.init(frame: frame)
    setupLayout()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

// MARK: Setup Methods
private extension FavouritePodcastCell {
  func updateLayout(with podcast: Podcast?) {
    nameLabel.text = podcast?.trackName
    artistNameLabel.text = podcast?.artistName

    let url = URL(string: podcast?.artworkUrl600 ?? "")
    imageView.sd_setImage(with: url)
  }

  func setupLayout() {
    backgroundColor = .white

    imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor).isActive = true
    // Content hugging = content does not want to grow
    // Compression resistance = content does not want to shrink
    // By setting a lower priority (default is 250), this artistNameLabel will grow since the nameLabel has a higher priority
//    artistNameLabel.setContentHuggingPriority(UILayoutPriority.init(rawValue: 249), for: .vertical)
    let stackView = UIStackView(arrangedSubviews: [imageView, nameLabel, artistNameLabel])
    stackView.translatesAutoresizingMaskIntoConstraints = false
    stackView.axis = .vertical
    stackView.spacing = 8

    addSubview(stackView)
    stackView.fillSuperview()


  }
}
