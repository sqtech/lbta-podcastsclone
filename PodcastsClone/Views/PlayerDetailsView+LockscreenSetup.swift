//
//  PlayerDetailsView+LockscreenSetup.swift
//  PodcastsClone
//
//  Created by Simon Quach on 2018-06-11.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit
import AVKit
import MediaPlayer

extension PlayerDetailsView {
  /// Sets up the lockscreen elapsed time
  /// - parameter time: The time in seconds to pass into to display on the lockscreen (i.e. for when user drags slider and/or presses forward/rewind buttons). Defaults to the current player time
  func setupLockscreenElapsedTime(for time: Float64? = nil, playbackRate: Float = 0) {
    let elapsedTime =  time ?? CMTimeGetSeconds(player.currentTime())
    MPNowPlayingInfoCenter.default().nowPlayingInfo?[MPNowPlayingInfoPropertyElapsedPlaybackTime] = elapsedTime

    MPNowPlayingInfoCenter.default().nowPlayingInfo?[MPNowPlayingInfoPropertyPlaybackRate] = playbackRate
  }

  func setupLockscreenDuration() {
    guard let currentItem = player.currentItem else { return }
    let durationInSeconds = CMTimeGetSeconds(currentItem.duration)
    MPNowPlayingInfoCenter.default().nowPlayingInfo?[MPMediaItemPropertyPlaybackDuration] = durationInSeconds

  }

  func setupNowPlayingInfoForLockScreen() {
    var nowPlayingInfo = [String: Any]()
    nowPlayingInfo[MPMediaItemPropertyTitle] = episode.title
    nowPlayingInfo[MPMediaItemPropertyArtist] = episode.author
    //    nowPlayingInfo[MPMediaItemArtwork]

    // MP = Media Player?
    MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo
  }

  /// Required to play audio inside notification center
  func setupRemoteControl() {
    UIApplication.shared.beginReceivingRemoteControlEvents()
    let commandCenter = MPRemoteCommandCenter.shared()
    commandCenter.playCommand.isEnabled = true
    commandCenter.playCommand.addTarget { _ -> MPRemoteCommandHandlerStatus in
      //      print("Should play podcast")
      self.player.play()
      self.updatePlayerLayout(for: .play)
      self.setupLockscreenElapsedTime(playbackRate: 1)
      return MPRemoteCommandHandlerStatus.success
    }
    commandCenter.pauseCommand.isEnabled = true
    commandCenter.pauseCommand.addTarget { _ -> MPRemoteCommandHandlerStatus in
      //      print("Should pause podcast")
      self.player.pause()
      self.updatePlayerLayout(for: .paused)
      self.setupLockscreenElapsedTime(playbackRate: 0)
      return MPRemoteCommandHandlerStatus.success
    }

    // Allows user to use their headphones play/pause button
    commandCenter.togglePlayPauseCommand.isEnabled = true
    commandCenter.togglePlayPauseCommand.addTarget { _ -> MPRemoteCommandHandlerStatus in
      self.handlePlayPause()
      return .success
    }

    commandCenter.previousTrackCommand.addTarget(self, action: #selector(handlePreviousTrack))
    commandCenter.nextTrackCommand.addTarget(self, action: #selector(handleNextTrack))

  }

  /// Required for background audio
  func setupAudioSession() {
    do {
      try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
      try AVAudioSession.sharedInstance().setActive(true)
    } catch {
      print("Failed to activate session:", error)
    }
  }
}
