//
//  EpisodeCell.swift
//  PodcastsClone
//
//  Created by Simon Quach on 2018-05-23.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit

class EpisodeCell: UITableViewCell {
  @IBOutlet weak var episodeImageView: UIImageView!
  @IBOutlet weak var pubDateLabel: UILabel!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var descriptionLabel: UILabel!
  @IBOutlet weak var progressLabel: UILabel!

//  lazy var dateFormatter: DateFormatter = {
//    let dateFormatter = DateFormatter()
//    dateFormatter.dateFormat = "MMM dd, yyy"
//    return dateFormatter
//  }()

  var episode: Episode! {
    didSet {
      setupLayout()
    }
  }

}

private extension EpisodeCell {
  func setupLayout() {
    titleLabel.text = episode.title
    descriptionLabel.text = episode.description
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "MMM dd, yyy"

    pubDateLabel.text = dateFormatter.string(from: episode.pubDate)
    let url = URL(string: episode.imageUrl?.toSecureHTTPS() ?? "")
    episodeImageView.sd_setImage(with: url)
  }
}
