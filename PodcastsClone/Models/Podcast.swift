//
//  Podcast.swift
//  PodcastsClone
//
//  Created by Simon Quach on 2018-05-20.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import Foundation

// Conform to NSObject and NSCoding to allow persistant storage
class Podcast: NSObject, Codable, NSCoding {
  var trackName: String?
  var artistName: String?
  var artworkUrl600: String?
  var trackCount: Int?
  var feedUrl: String?

  func encode(with aCoder: NSCoder) {
    aCoder.encode(trackName ?? "", forKey: "trackNameKey")
    aCoder.encode(artistName ?? "", forKey: "artistNameKey")
    aCoder.encode(artworkUrl600 ?? "", forKey: "artworkUrl600Key")
    aCoder.encode(feedUrl ?? "", forKey: "feedUrlKey")
  }

  required init?(coder aDecoder: NSCoder) {
    self.trackName = aDecoder.decodeObject(forKey: "trackNameKey") as? String
    self.artistName = aDecoder.decodeObject(forKey: "artistNameKey") as? String
    self.artworkUrl600 = aDecoder.decodeObject(forKey: "artworkUrl600Key") as? String
    self.feedUrl = aDecoder.decodeObject(forKey: "feedUrlKey") as? String
  }
}
