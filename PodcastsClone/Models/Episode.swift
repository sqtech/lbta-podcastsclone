//
//  Episode.swift
//  PodcastsClone
//
//  Created by Simon Quach on 2018-05-23.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import Foundation
import FeedKit

struct Episode: Codable {
  let title: String
  let pubDate: Date
  let description: String
  var imageUrl: String?
  let author: String
  let streamUrl: String
  var fileUrl: String?

  init(feedItem: RSSFeedItem) {
    self.title = feedItem.title ?? ""
    self.pubDate = feedItem.pubDate ?? Date()
    self.description = feedItem.iTunes?.iTunesSubtitle ?? feedItem.description ?? ""
    self.imageUrl = feedItem.iTunes?.iTunesImage?.attributes?.href
//    self.imageUrl = feedItem.iTunes?.iTunesImage?.attributes?.href ?? "" // this caused the same images to not load... DANGEROUS!
    self.author = feedItem.iTunes?.iTunesAuthor ?? ""
    self.streamUrl = feedItem.enclosure?.attributes?.url ?? ""
  }
}
