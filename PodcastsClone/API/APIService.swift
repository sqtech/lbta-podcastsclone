//
//  APIService.swift
//  PodcastsClone
//
//  Created by Simon Quach on 2018-05-20.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import Foundation
import Alamofire
import FeedKit

class APIService {
  // Singleton
  static let shared = APIService()
  let baseiTunesSearchURL = "https://itunes.apple.com/search"

  /// Tuple that will contain the fileURL and episodeTitle
  typealias episodeDownloadComplete = (fileURL: String, episodeTitle: String)

  func download(episode: Episode) {
//    print("Downloading episode with alamofire")
    let destination = DownloadRequest.suggestedDownloadDestination()
    Alamofire.download(episode.streamUrl, to: destination).downloadProgress { progress in
      print(progress.fractionCompleted)
      // Notify DownloadsController of progress
      NotificationCenter.default.post(name: .DownloadProgress, object: nil, userInfo: ["title": episode.title, "progress": progress.fractionCompleted])
      }.response { response in
//        print(response.destinationURL?.absoluteString ?? "")
        let episodeDownloadCompleteTuple = episodeDownloadComplete(response.destinationURL?.absoluteString ?? "", episode.title)

        // Using object instead of userInfo now
        NotificationCenter.default.post(name: .DownloadComplete, object: episodeDownloadCompleteTuple, userInfo: nil)
        var downloadedEpisodes = UserDefaults.standard.downloadedEpisodes()
        guard let index = downloadedEpisodes.index(where: { $0.title == episode.title && $0.author == episode.author }) else { return }
        downloadedEpisodes[index].fileUrl = response.destinationURL?.absoluteString ?? ""
        do {
          let data = try JSONEncoder().encode(downloadedEpisodes)
          UserDefaults.standard.set(data, forKey: UserDefaults.downloadedEpisodeKey)
        } catch {
          print("Failed to encode downloadedEpisodes:", error)
        }
    }
//    Alamofire.download(episode.streamUrl).downloadProgress { progress in
//      print(progress.fractionCompleted)
//    }
  }

  func fetchEpisodes(feedUrl: String, then completionHandler: @escaping ([Episode]) -> Void) {
    guard let url = URL(string: feedUrl.toSecureHTTPS()) else { return }

    // Perform on background thread because parser takes a small delay
    DispatchQueue.global(qos: .background).async {
      let parser = FeedParser(URL: url)
      parser?.parseAsync(result: { result in
        if let err = result.error {
          print("Failed to parse XML feed:", err)
          return
        }

        print("Successfully parsed feed:", result.isSuccess)
        guard let feed = result.rssFeed else { return }
        let episodes = feed.toEpisodes()
        completionHandler(episodes)

        //      switch result {
        ////      case let .atom(feed):
        //      case let .rss(feed):
        //        self.episodes = feed.toEpisodes()
        //        DispatchQueue.main.async {
        //          self.tableView.reloadData()
        //        }
        ////      case let .json(feed):
        //      case let .failure(error):
        //        print("Failed to parse feed:", error)
        //      default:
        //        break
        //      }

      })
    }
  }

  func fetchPodcasts(with searchText: String, then completionHandler: @escaping ([Podcast]) -> Void) {
//    print("Searching for podcasts")
    let parameters = ["term": searchText, "media": "podcast"]

    Alamofire.request(baseiTunesSearchURL, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseData { (responseData) in
      if let err = responseData.error {
        print("Failed to contact iTunes", err)
        return
      }

      guard let data = responseData.data else { return }

      do {
        let searchResult = try JSONDecoder().decode(SearchResults.self, from: data)
        completionHandler(searchResult.results)
      } catch {
        print("Failed to decode:", error)
      }

    }
  }
}

/// The model to represent the data returned from the iTunes API
struct SearchResults: Decodable {
  let resultCount: Int
  let results: [Podcast]
}

extension Notification.Name {
  static let DownloadProgress = NSNotification.Name("downloadProgress")
  static let DownloadComplete = NSNotification.Name("downloadComplete")
}
