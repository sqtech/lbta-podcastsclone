//
//  String+Secure.swift
//  PodcastsClone
//
//  Created by Simon Quach on 2018-05-24.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import Foundation

extension String {
  func toSecureHTTPS() -> String {
    return self.contains("https") ? self : self.replacingOccurrences(of: "http", with: "https")
  }
}
