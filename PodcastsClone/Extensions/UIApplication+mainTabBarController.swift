//
//  UIApplication+mainTabBarController.swift
//  PodcastsClone
//
//  Created by Simon Quach on 2018-06-24.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit

extension UIApplication {
  static func mainTabBarController() -> MainTabBarController? {
    //UIApplication.shared.keyWindow?.rootViewController as? MainTabBarController

    return shared.keyWindow?.rootViewController as? MainTabBarController
  }
}
