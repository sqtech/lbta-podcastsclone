//
//  UserDefaults+savedPodcasts.swift
//  PodcastsClone
//
//  Created by Simon Quach on 2018-06-18.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import Foundation

extension UserDefaults {
  static let favouritedPodcastKey = "FavouritedPodcastKey"
  static let downloadedEpisodeKey = "DownloadedEpisodeKey"

  // MARK: Episodes
  func downloadedEpisodes() -> [Episode] {
    guard let episodesData = data(forKey: UserDefaults.downloadedEpisodeKey) else { return [] }

    do {
      let episodes = try JSONDecoder().decode([Episode].self, from: episodesData)
      return episodes
    } catch {
      print("Failed to decode episodes:", error)
    }

    return []
  }
  func download(episode: Episode) {
    var episodes = downloadedEpisodes()
//    episodes.append(episode)
    episodes.insert(episode, at: 0) // Put at top

    do {
      let data = try JSONEncoder().encode(episodes)
      UserDefaults.standard.setValue(data, forKey: UserDefaults.downloadedEpisodeKey)
    } catch {
      print("Failed to encode episode:", error)
    }
  }

  func delete(episode: Episode) {
    let episodes = downloadedEpisodes()
    let filteredEpisodes = episodes.filter { ep -> Bool in
      return ep.title != episode.title
    }

    do {
      let data = try JSONEncoder().encode(filteredEpisodes)
      UserDefaults.standard.setValue(data, forKey: UserDefaults.downloadedEpisodeKey)
    } catch {
      print("Failed to delete episode:", error)
    }

  }

  // MARK: Podcasts
  func savedPocasts() -> [Podcast] {
    guard
      let savedPodcastData = UserDefaults.standard.data(forKey: UserDefaults.favouritedPodcastKey),
      let podcasts = NSKeyedUnarchiver.unarchiveObject(with: savedPodcastData) as? [Podcast]
    else { return [] }

    return podcasts
  }

  func deletePodcast(podcast: Podcast) {
    let podcasts = savedPocasts()
    let filteredPodcasts = podcasts.filter { p -> Bool in
      return p.trackName != podcast.trackName || (p.trackName == podcast.trackName && p.artistName != podcast.artistName)
    }

    let data = NSKeyedArchiver.archivedData(withRootObject: filteredPodcasts)
    UserDefaults.standard.set(data, forKey: UserDefaults.favouritedPodcastKey)
  }
}
