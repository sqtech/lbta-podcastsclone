//
//  RSSFeed+toEpisodes.swift
//  PodcastsClone
//
//  Created by Simon Quach on 2018-05-25.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import FeedKit

extension RSSFeed {
  func toEpisodes() -> [Episode] {
    let imageUrl = iTunes?.iTunesImage?.attributes?.href
//        print("From rss feed", imageUrl ?? "")

    var episodes = [Episode]()
    items?.forEach({ feedItem in
      //          print(feedItem.iTunes?.iTunesImage?.attributes?.href)
      var episode = Episode(feedItem: feedItem)
      if episode.imageUrl == nil {
        episode.imageUrl = imageUrl
      }

      episodes.append(episode)
    })

    return episodes
  }
}
