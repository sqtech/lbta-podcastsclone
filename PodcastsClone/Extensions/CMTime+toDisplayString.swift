//
//  CMTime+toDisplayString.swift
//  PodcastsClone
//
//  Created by Simon Quach on 2018-05-30.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import AVKit

extension CMTime {
  func toDisplayString() -> String? {
//    print("Value:", value)
    if CMTimeGetSeconds(self).isNaN {
      return "--:--"
    }

    let totalSeconds = Int(CMTimeGetSeconds(self))
    let seconds = totalSeconds % 60
    let minutes = totalSeconds % (60 * 60) / 60
    let hours = totalSeconds / 60 / 60
    let timeFormatString = String(format: "%02d:%02d:%02d", hours, minutes, seconds)
    return timeFormatString
  }
}
