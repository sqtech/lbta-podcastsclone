//
//  MainTabBarController.swift
//  PodcastsClone
//
//  Created by Simon Quach on 2018-05-20.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController {

  let playerDetailsView = PlayerDetailsView.initFromNib()
  var maximizedTopAnchorConstraintForPlayerDetailsView: NSLayoutConstraint?
  var minimizedTopAnchorConstraintForPlayerDetailsView: NSLayoutConstraint?
  var bottomAnchorConstraint: NSLayoutConstraint?

  override func viewDidLoad() {
    super.viewDidLoad()

    UINavigationBar.appearance().prefersLargeTitles = true

    tabBar.tintColor = .purple

    setupViewControllers()
    setupPlayerDetailsView()
  }
}

// MARK: - Public Methods
extension MainTabBarController {
  func minimizePlayerDetails() {
    maximizedTopAnchorConstraintForPlayerDetailsView?.isActive = false
    bottomAnchorConstraint?.constant = view.frame.height
    minimizedTopAnchorConstraintForPlayerDetailsView?.isActive = true

    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
      self.view.layoutIfNeeded()
      self.transformTabBar(with: .identity)
      self.playerDetailsView.miniPlayerView.alpha = 1
      self.playerDetailsView.maximizedStackView.alpha = 0
    })
  }

  func maximizePlayerDetails(episode: Episode? = nil, playListEpisodes: [Episode] = []) {
    minimizedTopAnchorConstraintForPlayerDetailsView?.isActive = false
    maximizedTopAnchorConstraintForPlayerDetailsView?.isActive = true
    /// Setting the constant to 0 brings it all the way up to the top
    maximizedTopAnchorConstraintForPlayerDetailsView?.constant = 0
    bottomAnchorConstraint?.constant = 0

    if episode != nil {
      playerDetailsView.episode = episode
      playerDetailsView.playListEpisodes = playListEpisodes
    }

    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
      self.view.layoutIfNeeded()
      self.transformTabBar(with: CGAffineTransform(translationX: 0, y: 49))
      self.playerDetailsView.miniPlayerView.alpha = 0
      self.playerDetailsView.maximizedStackView.alpha = 1
    })
  }
}

// MARK: - Helper Methods
private extension MainTabBarController {
  func transformTabBar(with transform: CGAffineTransform) {
    self.tabBar.transform = transform
  }

  func setupPlayerDetailsView() {
    playerDetailsView.delegate = self
    playerDetailsView.translatesAutoresizingMaskIntoConstraints = false
    view.insertSubview(playerDetailsView, belowSubview: tabBar)

    // Start the view off frame by giving it a constant of the views height (which takes it to the very bottom)
    maximizedTopAnchorConstraintForPlayerDetailsView = playerDetailsView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: view.frame.height)
    maximizedTopAnchorConstraintForPlayerDetailsView?.isActive = true

    playerDetailsView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
    playerDetailsView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
    bottomAnchorConstraint = playerDetailsView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: view.frame.height)
    bottomAnchorConstraint?.isActive = true

    // Minimized version will be height of 64pt above the tab bar
    minimizedTopAnchorConstraintForPlayerDetailsView = playerDetailsView.topAnchor.constraint(equalTo: tabBar.topAnchor, constant: -64)
    //    minimizedTopAnchorConstraintForPlayerDetailsView = playerDetailsView.heightAnchor.constraint(equalToConstant: 64)
  }

  func setupViewControllers() {
    let favouritesController = FavouritesController(collectionViewLayout: UICollectionViewFlowLayout())
    let searchNavController = setupNavController(with: PodcastsSearchController(), title: "Search", image: #imageLiteral(resourceName: "search"))
    let favouritesNavController = setupNavController(with: favouritesController, title: "Favourites", image: #imageLiteral(resourceName: "favorites"))
    let downloadsNavController = setupNavController(with: DownloadsController(), title: "Downloads", image: #imageLiteral(resourceName: "downloads"))

    viewControllers = [
      searchNavController,
      favouritesNavController,
      downloadsNavController
    ]
  }

  func setupNavController(with rootViewController: UIViewController, title: String, image: UIImage) -> UINavigationController {
    let navController = UINavigationController(rootViewController: rootViewController)
//    navController.navigationBar.prefersLargeTitles = true
    rootViewController.navigationItem.title = title
    navController.tabBarItem.title = title
    navController.tabBarItem.image = image
    return navController
  }
}

// MARK: - PlayerDetailsViewDelegate
extension MainTabBarController: PlayerDetailsViewDelegate {
  func playerDetailsViewMaximizeView(_ playerDetailsView: PlayerDetailsView) {
    maximizePlayerDetails()
  }

  func playerDetailsViewMinimizeView(_ playerDetailsView: PlayerDetailsView) {
    minimizePlayerDetails()
  }
}
