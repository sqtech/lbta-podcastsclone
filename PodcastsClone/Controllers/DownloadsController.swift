//
//  DownloadsController.swift
//  PodcastsClone
//
//  Created by Simon Quach on 2018-06-25.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit

class DownloadsController: UITableViewController {
  struct CellIdentifiers {
    static let downloadCell = "DownloadCell"
  }

  var episodes = UserDefaults.standard.downloadedEpisodes()

  override func viewDidLoad() {
    super.viewDidLoad()
    setupLayout()
    setupObservers()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    episodes = UserDefaults.standard.downloadedEpisodes()
    tableView.reloadData()
  }

}

// MARK: Private Methods
private extension DownloadsController {
  func delete(episode: Episode) {
    UserDefaults.standard.delete(episode: episode)
  }

  @objc func handleDownloadProgress(notification: Notification) {
    // notification userInfo is set inside APIService
    guard
      let userInfo = notification.userInfo as? [String: Any],
      let progress = userInfo["progress"] as? Double,
      let title = userInfo["title"] as? String
    else { return }

    guard let row = self.episodes.index(where: { $0.title == title }) else { return }
    let indexPath = IndexPath(row: row, section: 0)
    let cell = tableView.cellForRow(at: indexPath) as! EpisodeCell
    cell.progressLabel.text = "\(Int(progress * 100))%"
    cell.progressLabel.isHidden = false

    if progress == 1 {
      cell.progressLabel.isHidden = true
    }

  }

  /// Handles the download complete from APIService download(_) method
  @objc func handleDownloadComplete(notification: Notification) {
    guard let episodeDownloadComplete = notification.object as? APIService.episodeDownloadComplete else { return }

    guard let index = self.episodes.index(where: { $0.title == episodeDownloadComplete.episodeTitle }) else { return }
    // Update the episode fileUrl. fileUrl is not known when finished downloading after selecting download and going back to this class while downloading.
    self.episodes[index].fileUrl = episodeDownloadComplete.fileURL
  }
}
// MARK: Setup Methods
private extension DownloadsController {
  func setupObservers() {
    NotificationCenter.default.addObserver(self, selector: #selector(handleDownloadProgress), name: .DownloadProgress, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(handleDownloadComplete), name: .DownloadComplete, object: nil)
  }

  func setupLayout() {
    tableView.backgroundColor = .white
    tableView.rowHeight = 134
    let nib = UINib(nibName: "EpisodeCell", bundle: nil)
    tableView.register(nib, forCellReuseIdentifier: CellIdentifiers.downloadCell)
  }
}

// MARK: UITableViewDataSource
extension DownloadsController {
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return episodes.count
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.downloadCell, for: indexPath) as! EpisodeCell
    cell.episode = self.episodes[indexPath.row]
    return cell
  }
}

// MARK: UITableViewDelegate
extension DownloadsController {
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let episode = episodes[indexPath.row]
    if let _ = episode.fileUrl {
      UIApplication.mainTabBarController()?.maximizePlayerDetails(episode: episode, playListEpisodes: episodes)
    } else {
      let alertController = UIAlertController(title: "File URL not found", message: "Can not find local file, play using stream URL instead?", preferredStyle: .actionSheet)

      let action = UIAlertAction(title: "Yes", style: .default) { _ in
        UIApplication.mainTabBarController()?.maximizePlayerDetails(episode: episode, playListEpisodes: self.episodes)
      }
      alertController.addAction(action)
      let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
      alertController.addAction(cancelAction)
      present(alertController, animated: true, completion: nil)
    }
  }

  override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
    let deleteAction = UITableViewRowAction(style: .default, title: "Delete") { (_, _) in
      let episode = self.episodes[indexPath.row]
      self.episodes.remove(at: indexPath.row)
      self.tableView.deleteRows(at: [indexPath], with: .automatic)
      self.delete(episode: episode)
    }

    return [deleteAction]

  }
}
