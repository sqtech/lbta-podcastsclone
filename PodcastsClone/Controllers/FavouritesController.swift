//
//  FavouritesController.swift
//  PodcastsClone
//
//  Created by Simon Quach on 2018-06-16.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit

class FavouritesController: UICollectionViewController {
  struct CellIdentifiers {
    static let favouriteCell = "FavouriteCell"
  }

  var podcasts = UserDefaults.standard.savedPocasts()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupLayout()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    podcasts = UserDefaults.standard.savedPocasts()
    collectionView?.reloadData()
    UIApplication.mainTabBarController()?.viewControllers?[1].tabBarItem.badgeValue = nil
  }
}

// MARK: Private Methods
private extension FavouritesController {
  @objc func handleLongPress(gesture: UILongPressGestureRecognizer) {
    let location = gesture.location(in: collectionView)
    guard let selectedIndexPath = collectionView?.indexPathForItem(at: location) else { return }
    print(selectedIndexPath.item)

    let alertController = UIAlertController(title: "Remove Podcast?", message: nil, preferredStyle: .actionSheet)
    let removeAction = UIAlertAction(title: "Yes", style: .destructive) { _ in
      // remove podcast here
      let selectedPodcast = self.podcasts[selectedIndexPath.item]
      self.podcasts.remove(at: selectedIndexPath.item)
      self.collectionView?.deleteItems(at: [selectedIndexPath])
      UserDefaults.standard.deletePodcast(podcast: selectedPodcast)
    }
    alertController.addAction(removeAction)

    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
    alertController.addAction(cancelAction)

    present(alertController, animated: true)
  }
}

// MARK: Setup Methods
private extension FavouritesController {
  func setupLayout() {
    collectionView?.backgroundColor = .white
    collectionView?.register(FavouritePodcastCell.self, forCellWithReuseIdentifier: CellIdentifiers.favouriteCell)

    let gesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
    collectionView?.addGestureRecognizer(gesture)
  }
}

// MARK: UICollectionViewDataSource
extension FavouritesController {
  override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return podcasts.count
  }

  override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifiers.favouriteCell, for: indexPath) as! FavouritePodcastCell

    cell.podcast = podcasts[indexPath.item]

    return cell
  }
}

// MARK: UICollectionViewDelegate
extension FavouritesController {
  override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let episodesController = EpisodesController()
    episodesController.podcast = self.podcasts[indexPath.item]
    navigationController?.pushViewController(episodesController, animated: true)
  }
}

// MARK: UICollectionViewDelegateFlowLayout
extension FavouritesController: UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    // width = (viewWidth - (numberOfItemsInRow + 1) * spacing) / numberOfItemsInRow
    // spacing between cells = numberOfItemsInRow + 1
    let width = (collectionView.frame.width - 3 * 16) / 2
    var height: CGFloat = width // imageView
    height += 30 // room for title and artist name
    height += 3 * 8 // spacing between number of views
    return CGSize(width: width, height: height)
  }

  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    return UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
  }

  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 16
  }
}
