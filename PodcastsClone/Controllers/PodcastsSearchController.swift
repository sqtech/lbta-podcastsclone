//
//  PodcastsSearchController.swift
//  PodcastsClone
//
//  Created by Simon Quach on 2018-05-20.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit
import Alamofire

class PodcastsSearchController: UITableViewController {
  struct CellIdentifiers {
    static let podcastCellId = "PodcastCellId"
  }

  // MARK: Properties
  var podcasts = [Podcast]()

  var searchTimer: Timer?

  lazy var searchController: UISearchController = {
    /// Set to nil because the navigation bar contains a searchController already
    let searchController = UISearchController(searchResultsController: nil)
    searchController.obscuresBackgroundDuringPresentation = false
    searchController.searchBar.delegate = self
    return searchController
  }()

  let activityIndicatorView: UIActivityIndicatorView = {
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    activityIndicator.color = .darkGray
    return activityIndicator
  }()


  override func viewDidLoad() {
    super.viewDidLoad()
    setupLayout()
  }
}

// MARK: Methods
private extension PodcastsSearchController {
  func setupLayout() {
    // prevents blank header when navigating to new page when current page has a search controller
    definesPresentationContext = true
    navigationItem.searchController = searchController
    navigationItem.hidesSearchBarWhenScrolling = false

//    tableView.register(UITableViewCell.self, forCellReuseIdentifier: CellIdentifiers.podcastCellId)
    tableView.tableFooterView = UIView() // Removes empty horizontal spaces
    let nib = UINib(nibName: "PodcastCell", bundle: nil)
    tableView.register(nib, forCellReuseIdentifier: CellIdentifiers.podcastCellId)
    tableView.rowHeight = 132
    configureTableBackgroundView()

    // Temporary
    #if DEBUG
      searchBar(searchController.searchBar, textDidChange: "Voong")
    #endif
  }

  func configureTableBackgroundView() {
    guard let searchText = searchController.searchBar.text else {
      return
    }

    // If search text is not empty we will display the currently searching footer view
    if podcasts.count > 0 || !searchText.isEmpty {
      tableView.backgroundView = nil
    } else {
      let label = UILabel()
      label.text = "Please enter a Search Term"
      label.textAlignment = .center
      label.textColor = .purple
      label.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
      tableView.backgroundView = label
    }
  }
}

// MARK: UISearchBarDelegate
extension PodcastsSearchController: UISearchBarDelegate {
  func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    // Everytime user performs text change, invalidate previous timer
    searchTimer?.invalidate()
    // If no changes have been made after 0.5s, search will initiate
    searchTimer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false, block: { timer in
      APIService.shared.fetchPodcasts(with: searchText, then: { podcasts in
      print("Finished fetching with podcasts:", podcasts)
        self.podcasts = podcasts
        self.configureTableBackgroundView()
        self.tableView.reloadData()
      })
    })
  }
}

// MARK: UITableViewDataSource
extension PodcastsSearchController {
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return podcasts.count
  }

  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.podcastCellId, for: indexPath) as! PodcastCell
    cell.backgroundColor = .white
    let podcast = podcasts[indexPath.row]
    cell.podcast = podcast
    
    return cell
  }
}

// MARK: UITableViewDelegate
extension PodcastsSearchController {
  override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    // Will display the default background view
    guard let searchText = searchController.searchBar.text, !searchText.isEmpty else {
      return nil
    }

    let footerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 200))
    footerView.addSubview(activityIndicatorView)
    activityIndicatorView.frame = CGRect(x: view.center.x, y: 0, width: 0, height: 60)
    let searchLabel = UILabel()
    searchLabel.text = "Currently Searching..."
    searchLabel.textColor = .purple
    searchLabel.textAlignment = .center
    searchLabel.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
    footerView.addSubview(searchLabel)
    searchLabel.frame = CGRect(x: 0, y: 62, width: view.frame.width, height: 20)
    return footerView
  }

  override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    if podcasts.isEmpty {
      activityIndicatorView.startAnimating()
    } else {
      activityIndicatorView.stopAnimating()
    }

    guard let searchText = searchController.searchBar.text, !searchText.isEmpty else {
      return 0
    }

    return podcasts.isEmpty ? 200 : 0
  }

  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let podcast = podcasts[indexPath.row]
    let episodesController = EpisodesController()
    episodesController.podcast = podcast
    navigationController?.pushViewController(episodesController, animated: true)
  }
}
