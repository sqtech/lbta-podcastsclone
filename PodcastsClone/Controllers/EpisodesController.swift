//
//  EpisodesController.swift
//  PodcastsClone
//
//  Created by Simon Quach on 2018-05-21.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit
import FeedKit

class EpisodesController: UITableViewController {
  struct CellIdentifiers {
    static let episodeCell = "EpisodeCell"
  }
  var podcast: Podcast? {
    didSet {
      fetchEpisodes()
    }
  }

  var episodes = [Episode]()

  let activityIndicatorView: UIActivityIndicatorView = {
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    activityIndicator.color = .darkGray
    return activityIndicator
  }()

  override func viewDidLoad() {
    super.viewDidLoad()
    setupLayout()
  }
}

// MARK: Private Methods
private extension EpisodesController {
  func showBadgeHighlight() {
    UIApplication.mainTabBarController()?.viewControllers?[1].tabBarItem.badgeValue = "New"
  }

  func download(episode: Episode) {
    UserDefaults.standard.download(episode: episode)
    APIService.shared.download(episode: episode)
  }

  func fetchEpisodes() {
    guard let feedUrl = podcast?.feedUrl else { return }
    APIService.shared.fetchEpisodes(feedUrl: feedUrl, then: { episodes in
      self.episodes = episodes

      DispatchQueue.main.async {
        self.tableView.reloadData()
      }
    })

  }

  @objc func handleSaveFavourite() {
    guard let podcast = podcast else { return }

    var podcasts = UserDefaults.standard.savedPocasts()

    // Transform Podcast into Data
    podcasts.append(podcast)
    let data = NSKeyedArchiver.archivedData(withRootObject: podcasts)
    UserDefaults.standard.set(data, forKey: UserDefaults.favouritedPodcastKey)
    showBadgeHighlight()
    setupHeartBarButtonIcon()
    // Using codable to encode
//    do {
//      let data = try JSONEncoder().encode(podcast)
//      UserDefaults.standard.set(data, forKey: favouritedPodcastKey)
//    } catch {
//      print("Failed to encode podcast:", error)
//    }
  }

  @objc func handleFetchSavePodcasts() {
//    let value = UserDefaults.standard.value(forKey: favouritedPodcastKey) as? String
    guard let data = UserDefaults.standard.data(forKey: UserDefaults.favouritedPodcastKey) else { return }
    let podcasts = NSKeyedUnarchiver.unarchiveObject(with: data) as? [Podcast]
    podcasts?.forEach({ print($0.trackName ?? "")})

    // Using codable to decode
//    do {
//      let podcast = try JSONDecoder().decode(Podcast.self, from: data)
//      print(podcast.artistName ?? "")
//    } catch {
//      print("Error decoding podcast", error)
//    }

  }
}

// MARK: Setup Methods
private extension EpisodesController {
  func setupHeartBarButtonIcon() {
    navigationItem.rightBarButtonItems = nil
    navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "heart"), style: .plain, target: nil, action: nil)
  }

  func setupNavigationBarButtons() {
    let savedPodcasts = UserDefaults.standard.savedPocasts()
    let hasFavourited = savedPodcasts.index(where: { $0.trackName == self.podcast?.trackName && $0.artistName == self.podcast?.artistName }) != nil
    if hasFavourited {
      setupHeartBarButtonIcon()
    } else {
      navigationItem.rightBarButtonItems = [
        UIBarButtonItem(title: "Favourite", style: .plain, target: self, action: #selector(handleSaveFavourite)),
        UIBarButtonItem(title: "Fetch", style: .plain, target: self, action: #selector(handleFetchSavePodcasts))
      ]
    }


  }

  func setupLayout() {
    navigationItem.title = podcast?.trackName ?? "Episodes"
    tableView.tableFooterView = UIView()
    tableView.rowHeight = 132
    let nib = UINib(nibName: "EpisodeCell", bundle: nil)
    tableView.register(nib, forCellReuseIdentifier: CellIdentifiers.episodeCell)

    setupNavigationBarButtons()
  }
}

// MARK: UITableViewDataSource
extension EpisodesController {
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return episodes.count
  }

  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.episodeCell, for: indexPath) as! EpisodeCell
    let episode = episodes[indexPath.row]
    cell.episode = episode
    return cell

  }

}

// MARK: UITableViewDelegate
extension EpisodesController {
  override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    return activityIndicatorView
  }

  override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    if episodes.isEmpty {
      activityIndicatorView.startAnimating()
    } else {
      activityIndicatorView.stopAnimating()
    }

    return episodes.isEmpty ? 200 : 0
  }

  override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
    let downloadAction = UITableViewRowAction(style: .default, title: "Download") { (_, _) in
      print("Downloading episode into UserDefaults")
      let episode = self.episodes[indexPath.row]
      self.download(episode: episode)
    }

    return [downloadAction]
  }

  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let episode = episodes[indexPath.row]
    print("Trying to play episode:", episode.title)

    // Player details view is instantiated inside MainTabBarController
//    let mainTabBarController = UIApplication.shared.keyWindow?.rootViewController as? MainTabBarController
    UIApplication.mainTabBarController()?.maximizePlayerDetails(episode: episode, playListEpisodes: self.episodes)

//    let window = UIApplication.shared.keyWindow
//    // Call first to get the first View inside the xib file
//    let playerDetailsView = PlayerDetailsView.initFromNib()
//    playerDetailsView.episode = episode
//    playerDetailsView.frame = view.frame
//    window?.addSubview(playerDetailsView)

  }
}







